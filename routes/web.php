<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/curtains');
});

Route::resource('curtains', 'CurtainsSettingsController');

Route::get('curtains/{id}/calculate', 'CurtainsSettingsController@calculate');
Route::get('curtains/{id}/complete', 'CurtainsSettingsController@complete');
Route::get('curtains/{id}/error', 'CurtainsSettingsController@error');
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');
