@extends('layouts.app')
@section("extra_style")
<link rel="stylesheet" href="{{ asset('css/curtains/show_style.css') }}">
<link rel="stylesheet" href="{{ asset('css/curtains/loading_anim.css') }}">
@endsection

@section('content')

    <h1>{{ $setting->curtains_name }}</h1>
    <div class="float-right">
        <a href='/curtains/{{ $setting->id }}/edit' class="btn btn-warning">Edit</a>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="visual_page_block @if( $setting->isPageLandscape()) landscape @endif">
                <div class="width_sign"></div>
                <div class="height_sign"></div>
                <div class="width">
                    {{ $setting->page_width}}
                </div>
                <div class="height">
                    {{ $setting->page_height}}
                </div>
                @if($setting->page_pattern)
                    <div class="pattern">
                        <i class="fas fa-arrow-up"></i>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-6">
            <table class="table table-striped" style="background-color: #ffffff">
                <tr>
                    <th>#</th>
                    <th>Larghezza</th>
                    <th>Altezza</th>
                    <th>Quantità</th>
                </tr>
                @foreach ($setting->get_curtains_form_list() as $curtain)
                    <tr>
                        <td>
                            {{ $curtain['curt_id']}}
                        </td>
                        <td>
                            {{ $curtain['w']}}
                        </td>
                        <td>
                            {{ $curtain['h']}}
                        </td>
                        <td>
                            {{ $curtain['q']}}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="sub_section_title my-1 p-2">
                Disposizione Tende:
                @if($setting->curtains_best_filling == null)
                    @if($setting->calculating)
                        <a href="/curtains/{{ $setting->id}}/calculate" id="calculating" data-curtid="{{ $setting->id }}" class="btn btn-info btn-sm mx-3 float-right disabled">Sto Calcolando...</a>
                    @else
                        <a href="/curtains/{{ $setting->id}}/calculate" id="calculate_settings" data-curtid="{{ $setting->id }}" class="btn btn-success btn-sm mx-3 float-right">Calcola</a>
                    @endif
                @endif
            </div>
        </div>
        <div id="list_of_pages" style="margin-top: 20px;">
            @if($setting->curtains_best_filling != null)
                <div class="row">
                    @foreach ($setting->get_curtains_filling() as $item)
                        <div class="col">
                            <div class="page_base" style="width: {{ $setting->get_base()->get_width_html() }}px; height: {{ $setting->get_base()->get_height_html() }}px;">
                                @foreach ($item->curtains as $rect)
                                    <div class="curtain_shape" style="top:{{ $rect->get_y_html() }}px; left:{{ $rect->get_x_html() }}px; width:{{ $rect->get_width_html() }}px; height:{{ $rect->get_height_html() }}px; line-height:{{ $rect->get_height_html() }}px;" >{{ $rect->curtain_id }}</div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div id="box_loading_container">
        <div id="box_loading_text">Sto elaborando {{ $setting->combination_number }} Combinazioni.... Riprova più tardi<br /><a href="">Ricarica Pagina</a></div>
        <div id="box_loading">
            <div class="big_rect" style=""></div>
            <div class="rect pink"></div>
            <div class="rect purple"></div>
            <div class="rect blue"></div>
            <div class="rect green"></div>
        </div>
    </div>
@endsection

@section('hidden_section')
@if($setting->page_pattern)
    <div id="page_with_pattern"></div>
@endif
<div id='order_number_of_curtains'>
    {{ $setting->num_of_curtains() }}
</div>
@endsection

@section("extra_script")
<script type="text/javascript">
    function start_loading(){
        document.getElementById('box_loading_container').className = 'active';
        document.getElementById('list_of_pages').innerHTML = "";
        
        var summ = parseInt(document.getElementById('order_number_of_curtains').innerHTML.trim());
        if (document.getElementById('page_with_pattern') != undefined)
            summ = summ/2 +2
        var fat = 1;
        for (var i =1; i < summ; i++)
            fat = fat*i;
        document.getElementById('box_loading_text').innerHTML = 'Attendere! Sto elaborando ' + fat + " Combinazioni!..";
    }

    $(document).ready(function(){
        function call_calculating(href,curtid){
            $.ajax({
                url: href,
                type: "GET",
                data: {
                    ajax: 'yes'
                }

            }).done(function(response) {
                let obj = JSON.parse(response);
                if (obj.response_code == 200){
                    location.href="/curtains/"+curtid+"/complete";
                }else if (obj.response_code == 500){
                        location.href="/curtains/"+curtid+"/error?msg='"+obj.msg+"'";
                    } else {
                        alert("Sto Ancora Elaborando.. La preghiamo di attendere");
                    }
            });
        }
        $("#calculate_settings").on("click", function(){
            start_loading();
            call_calculating(this.href, $(this).data("curtid"));
            return false;
        });

        if ($("#calculating").length > 0) {
            call_calculating($("#calculating").first().attr("href"), $("#calculating").first().data("curtid"));
        }
    });

</script>
@endsection