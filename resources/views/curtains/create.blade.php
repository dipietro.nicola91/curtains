@extends('layouts.app')
@section('extra_style')
<link rel="stylesheet" href="{{ asset('css/curtains/create_style.css') }}">
@endsection

@include('curtains.extra.script')

@section('content')

    <h1>Crea Ordine</h1>
    {{ old('test_something')}}
    <form action="{{ action('CurtainsSettingsController@store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name='count_curtain' id='count_curtain' value='{{ old('count_curtain')}}' />
        <div class="row">
            <div class="col">
                <input type="text" class="form-control input-md" name="setting_name" placeholder="save as.. (ex. Order number 1234)" value='{{ old('setting_name')}}'>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12">
                <div class="row">
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Larghezza</div>
                            </div>
                            <input class="form-control input-md" id="page_width" type="text" name="page_width" placeholder="ex. 2500 sono 2.5mt" value='{{ old('page_width')}}'>
                            <div class="input-group-append">
                                <div class="input-group-text">mm</div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Altezza</div>
                            </div>
                            <input class="form-control input-md" id="page_height" type="text" name="page_height" placeholder="ex. 3000 sono 3mt" value='{{ old('page_height')}}'>
                            <div class="input-group-append">
                                <div class="input-group-text">mm</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto my-1">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="autoSizingCheck2">
                            <input class="form-check-input form_label" type="checkbox" name="page_with_pattern" id="pattern_checkbox">
                            <label class="form-check-label form_label" for="pattern_checkbox">
                                Con Trama {{ old('count_curtain')}}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-list" id='list_of_curtains'>
                    @if(old('count_curtain') != null)
                        <?php for($i=0; $i < intval(old('count_curtain')) ; $i++): ?>
                            <div class="curtain-form-group"> 
                                <div class="row">
                                    <div class="col-1"></div>
                                    <div class="col-3">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">W</div>
                                            </div>
                                            <input class="form-control input-md" type="text" name="width[]" placeholder="Larghezza" value="{{ old('width')[$i] }}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">H</div>
                                            </div>
                                            <input class="form-control input-md" type="text" name="height[]" placeholder="Altezza" value="{{ old('height')[$i] }}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Q</div>
                                            </div>
                                            <input class="form-control input-md" type="text" name="quantity[]" placeholder="Quantità" value="{{ old('quantity')[$i] }}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <a href="#" class="btn btn-danger" onclick="removeFormGroup(this); return false;">Elimina</a>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <div style="text-align: center;">
                    <a href="" class="btn btn-success add_dimension_button">Aggiungi</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 pull-right" style="text-align: right;">
                <input class="btn btn-primary btn-lg" type="submit" value="Salva">
            </div>
        </div>
    </form>
@endsection

@section('hidden_section')
<div id="base_form" class="curtain-form-group">
    <div class="row">
        <div class="col-1"></div>
        <div class="col-3">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">W</div>
                </div>
                <input class="form-control input-md" type="text" name="width[]" placeholder="Larghezza">
                <div class="input-group-append">
                    <div class="input-group-text">mm</div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">H</div>
                </div>
                <input class="form-control input-md" type="text" name="height[]" placeholder="Altezza">
                <div class="input-group-append">
                    <div class="input-group-text">mm</div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Q</div>
                </div>
                <input class="form-control input-md" type="text" name="quantity[]" placeholder="Quantità">
                <div class="input-group-append">
                    <div class="input-group-text">mm</div>
                </div>
            </div>
        </div>
        <div class="col-2">
            <a href="" class="btn btn-danger" onclick="removeFormGroup(this); return false;">Elimina</a>
        </div>
    </div>
</div>
@endsection