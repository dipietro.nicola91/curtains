@section("extra_script")
<script type="text/javascript">
    function start_loading(){
        document.getElementById('box_loading_container').className = 'active';
        document.getElementById('list_of_pages').innerHTML = "";
        var qt = document.getElementsByName('quantity[]')
        var summ = 0;
        for (var i=0;i<qt.length;i++){
            var qtt = qt[i].value;
            if (qtt != "")
                summ += parseInt(qtt)
        }
        if (document.getElementsByName('page_with_pattern')[0].checked)
            summ = summ/2 +2
        var fat = 1;
        for (var i =1; i < summ; i++)
            fat = fat*i;
        console.log(fat)
        document.getElementById('box_loading_text').innerHTML = 'Attendere! Sto elaborando ' + fat + " Combinazioni!..";
    }
    function addForm(ev){
        var form = document.getElementById("base_form").cloneNode(true);
        form.id = "";
        var where_to_append = document.getElementsByClassName("form-list")[0];
        where_to_append.append(form);
        document.getElementById('count_curtain').value = where_to_append.children.length;
        
        ev.stopPropagation()
        return false;
    }
    document.getElementsByClassName("add_dimension_button")[0].onclick = addForm

    
    function removeFormGroup(that){
        that.parentElement.parentElement.remove();
        return false;
    }
</script>
@endsection