@extends('layouts.app')

@section("extra_style")
    <link rel="stylesheet" href="{{ asset('css/curtains/index_style.css') }}">
@endsection

@section('content')
    <h1>Tutti gli ordini</h1>
    @if(count($settings) > 0)
        @foreach ($settings as $setting)
        <div class="card p-3">
            <div class="row">
                <div class="col">
                    <div class="order_title">
                        <div class="item"><a href='/curtains/{{ $setting->id }}'>{{ $setting->curtains_name }}</a></div>
                        <small class="text-muted">Created on {{ $setting->created_at->format('d-m-Y') }} by <b>{{ $setting->user->name }}</b> </small>
                    </div>
                </div>
                <div class="col-md-1 col-sm-2">
                    <div class="order_info">
                        <div class="item">{{ $setting->page_width }}</div>
                        <small>Larghezza</small>
                    </div>
                </div>
                <div class="col-md-1 col-sm-2">
                    <div class="order_info">
                        <div class="item">{{ $setting->page_height }}</div>
                        <small>Altezza</small>
                    </div>
                </div>
                <div class="col-md-1 col-sm-2">
                    <div class="order_info">
                        <div class="item">{{ $setting->num_of_curtains() }} </div>
                        Tende
                    </div>
                </div>
                <div class="col-md-1 col-sm-2">
                    <div class="order_info">
                        @if($setting->calculating)
                            <div class="icon loading">
                                <i class="fas fa-spinner"></i>
                            </div>
                        @else
                            @if($setting->curtains_best_filling != null)
                                <div class="icon completed">
                                    <i class="far fa-check-circle"></i>
                                </div>
                            @else
                                <div class="icon empty">
                                    <i class="far fa-circle"></i>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="col text-right">
                    <a href='/curtains/{{ $setting->id }}/edit' class="btn btn-warning mx-3">Modifica</a>
                </div>
            </div>
        </div>
        @endforeach
        {{ $settings->links() }}
    @else
        <p>No posts found</p>
    @endif
@endsection