<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Curtains Calculator') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css" integrity="sha384-i1LQnF23gykqWXg6jxC2ZbCbUMxyw5gLZY6UiUS98LYV5unm8GWmfkIS6jqJfb4E" crossorigin="anonymous">
    <style>
        .hidden{
            display: none;
        }
    </style>
    
    @yield('extra_style')
</head>
<body>
    <div id="app">
        
        <main class="py-4">
                @include('inc.navbar')

                <main class="container">
                    @include('inc.messages')
                    @yield('content')
                </main>
        </main>
    </div>
    
    <div class="hidden">
        @yield('hidden_section')
    </div>

    <script>
        /*var msgs = Array.from(document.getElementsByClassName("alert-message"));
        msgs.forEach(function(msg){
            setTimeout(function(){ msg.style.display = 'none' }, 3000);
        })*/
    </script>
    
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    @yield('extra_script')
</body>
</html>
