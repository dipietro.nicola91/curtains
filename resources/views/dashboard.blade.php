@extends('layouts.app')

@section("extra_style")
<style>
    h2.card-title a,h2.card-title a:hover{
        text-decoration: none !important;
    }
    .table td{
        line-height: 30px;
    }
    .icon{
        font-size: 24px;
    }
    .icon.completed{
        color: #1b5e20;
    }
    .icon.empty{
        color: #3490dc;
    }
    .icon.loading{
        color: #fb8c00;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1>I Miei Ordini</h1>
                    <a href='/curtains/create' class='btn btn-primary mb-4'>Nuovo Ordine</a>
                    <table class="table table-striped">
                        <tr>
                            <th>Nome Ordine</th>
                            <th>Data</th>
                            <th>Stato</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @if(count($settings) > 0)
                            @foreach ($settings as $setting)
                                <tr>
                                    <td>
                                        <a href='/curtains/{{ $setting->id }}'>{{ $setting->curtains_name }}</a>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $setting->created_at->isoFormat('D MMMM YYYY') }}</small>
                                    </td>
                                    <td>
                                        @if($setting->calculating)
                                            <div class="icon loading">
                                                <i class="fas fa-spinner"></i>
                                            </div>
                                        @else
                                            @if($setting->curtains_best_filling != null)
                                                <div class="icon completed">
                                                    <i class="far fa-check-circle"></i>
                                                </div>
                                            @else
                                                <div class="icon empty">
                                                    <i class="far fa-circle"></i>
                                                </div>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        <a href='/curtains/{{ $setting->id }}/edit' class="btn btn-warning">Modifica</a>
                                    </td>
                                    <td>
                                        <form action="{{ action('CurtainsSettingsController@destroy', $setting->id) }}" method="POST" class='float-right'>
                                            @method('DELETE')
                                            @csrf
                                            <input type="submit" class='btn btn-danger' value='Elimina' />
                                        </form>
                                    </td>
                                </div>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">
                                    Nessun post creato
                                </td>
                            </tr>
                        @endif
                        </table>
                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
