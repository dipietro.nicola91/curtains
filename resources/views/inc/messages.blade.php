@if(count($errors) > 0)
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-message">{{ $error }}</div>
    @endforeach
@endif

@if(session('success'))
    <div class="alert alert-success alert-message">
        {{ session('success') }}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger alert-message">
        {{ session('error') }}
    </div>
@endif