<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Processing\PagesFillingsProcess;
use App\CurtainsSetting;


class CurtainsProcessing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $setting;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CurtainsSetting $setting)
    {
        $this->setting = $setting;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page_filling_processing = new PagesFillingsProcess();
        $page_filling_processing->calculate($this->setting);
    }
}
