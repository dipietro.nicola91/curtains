<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

use App\Support\Base;
use App\Support\Rect;
use App\Support\Filling;

class CurtainsSetting extends Model
{
    public function get_base()
    {
        return new Base(intval($this->page_width),intval($this->page_height),$this->page_pattern,0,0);
    }

    public function get_curtains_list()
    {
        $list_of_rect = array();
        $id_counter = 1;
        $curtain_id = 1;
        $curtains_parsed = json_decode($this->curtains_form_json, true);
        foreach ($curtains_parsed as $el)
        {
            $w = intval($el['w']);
			$h = intval($el['h']);
            $q = intval($el['q']);
            for ($i=0;$i<$q;$i++){
                array_push($list_of_rect, new Rect($id_counter, $w, $h, $curtain_id, 0, 0, false));
                $id_counter++;
            }
            $curtain_id++;
        }
        
        return $list_of_rect;
    }
    
    public function get_curtains_form_list()
    {
        $list_of_info = array();
		$id_counter = 1;
        $curtains_parsed = json_decode($this->curtains_form_json, true);
        foreach ($curtains_parsed as $el)
        {
            $w = intval($el['w']);
			$h = intval($el['h']);
            $q = intval($el['q']);
            array_push($list_of_info, array("curt_id" => $id_counter, "w" => $w, "h" => $h, "q" => $q, "area" => ($w*$h) ) );
            $id_counter++;
        }
        
        return $list_of_info;
    }

    public function get_curtains_filling()
    {
        $best_fill = array();
        if ($this->curtains_best_filling != null)
        {
            $fillings = json_decode($this->curtains_best_filling, true);
            foreach ($fillings as $fill)
            {
                $temp = new Filling(array(), null, null);
                foreach ($fill['curtains'] as $rec)
                {
                    $temp_rec = new Rect($rec['id'], $rec['width'], $rec['height'], $rec['curtain_id'], $rec['x'], $rec['y'], $rec['landscape']);
                    array_push($temp->curtains, $temp_rec);
                }
                array_push($best_fill, $temp);
            }
        }
        return $best_fill;
    }

    public function isPageLandscape(){
        return $this->page_height < $this->page_width;
    }
    public function num_of_curtains()
    {
        return array_reduce(json_decode($this->curtains_form_json, true),function($res, $a) { return $res + $a['q'];  }, 0); 
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
