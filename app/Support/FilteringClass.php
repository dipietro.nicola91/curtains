<?php

namespace App\Support;

class FilteringClass
{
    var $rect;
    var $base;
    var $curt;

    public function not_fitting_filter_function_build($rect, $base){
        $this->rect = $rect;
        $this->base = $base;
        
        return function( $el )
        {
            $check = True;
            $check = $check && ($this->rect->width + $el->width > $this->base->width);
            $check = $check && ($this->rect->height + $el->height > $this->base->height);
            $base = $this->base;
            if (!$base->pattern)
            {
                $check = $check && ($this->rect->width +$el->height > $this->base->width);
                $check = $check && ($this->rect->height + $el->width > $this->base->width);
                $check = $check && ($this->rect->height + $el->height > $this->base->width);
                
                $check = $check && ($this->rect->height + $el->width > $this->base->height);
                $check = $check && ($this->rect->width + $el->height > $this->base->height);
                $check = $check && ($this->rect->width + $el->width > $this->base->height);
            }
            
            return $check;
        };
    }

    public function all_but_one_filter_function_build($curt){
        $this->curt = $curt;
        
        return function($x) { 
            return $x->id != $this->curt->id; 
        };
    }
}
