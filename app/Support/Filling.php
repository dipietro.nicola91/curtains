<?php

namespace App\Support;

class Filling
{
    var $curtains;
	var $util;
    var $trash;
    
   function __construct($curtains, $util, $trash)
   {
       $this->curtains = $curtains;
       $this->util = $util;
       $this->trash = $trash;
   }

   private function serialize_rect( $obj )
   {
       return '{"id": '.$obj->id.',"width": "'.$obj->width.'","height": "'.$obj->height.'", "curtain_id": "'.$obj->curtain_id.'","x": "'.$obj->x.'","y": "'.$obj->y.'","landscape": "'.$obj->landscape.'"}';
   }
   
   public function __toString()
   {
        $str = "Curtains: ";
        foreach ($this->curtains as $cur)
            $str = $str.$cur.", ";
        return $str." Util: ".$this->util. " Trash: ".$this->trash;
   }

   public function serilize_curtains()
   {
       if ($this->curtains != null){
           return array_map(serialize_rect, $this->curtains);
        }
        return '';
    }
}
