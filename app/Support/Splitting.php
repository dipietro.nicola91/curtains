<?php

namespace App\Support;

class Splitting
{
    var $base_one = null;
	var $base_two = null;
    var $class_type = null;

    function __construct($base_one, $base_two, $class_type)
    {
        $this->base_one = $base_one;
        $this->base_two = $base_two;
        $this->class_type = $class_type;
    }
    public function __toString()
    {
        return "base_one: ".$this->base_one->toString().", base_two: ".$this->base_two->toString();
    }
}
