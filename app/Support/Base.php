<?php

namespace App\Support;

use Illuminate\Support\Facades\Log;
use App\Support\Splitting;

class Base
{
    var $width = null;
	var $height = null;
	var $pattern = false;
	var $x = null;
	var $y = null;

    function __construct($width, $height, $pattern, $x, $y)
    {
        $this->width = intval($width);
        $this->height = intval($height);
        $this->pattern = $pattern;
        $this->x = intval($x);
        $this->y = intval($y);
    }
    
    public function split_on_rect( $rect )
    {
        $baseT_one = new Base($rect->width,$this->height - $rect->height, $this->pattern, $this->x, $this->y + $rect->height);
        $baseT_two = new Base($this->width - $rect->width, $this->height, $this->pattern, $this->x + $rect->width, $this->y);
        
        $baseL_one = new Base($this->width - $rect->width, $rect->height, $this->pattern, $this->x + $rect->width, $this->y);
        $baseL_two = new Base($this->width, $this->height - $rect->height, $this->pattern, $this->x, $this->y + $rect->height);
        
        //Log::info("SPLITTING");
        //Log::info("RECT");
        //Log::info($rect);
        //Log::info("BASE");
        //Log::info($this);
        //Log::info("SPLIT ONE");
        //Log::info($baseT_one);
        //Log::info($baseT_two);
        //Log::info("SPLIT TWO");
        //Log::info($baseL_one);
        //Log::info($baseL_two);
        //Log::info("END SPLITTING");
        return array(new Splitting($baseT_one, $baseT_two,'classT'),new Splitting($baseL_one, $baseL_two,'classL'));
    }

    public function get_area()
    {
        return $this->width * $this->height;
    }

    
    public function clone()
    {
        return Base($this->width, $this->height, $this->pattern, $this->x, $this->y);
    }
    
    public function __toString()
    {
        return "w:".$this->width." - h:".$this->height." - x:".$this->x." - t:".$this->y;
    }
    
    public function get_width_html()
    {
        return $this->width / 10;
    }

    public function get_height_html()
    {
        return $this->height / 10;
    }

    public function get_x_html()
    {
        return $this->x / 10;
    }
    
    public function get_y_html()
    {
        return $this->y / 10;
    }
    
    public function serialize()
    {
        return '{
            "width": "'.$this->width.'",
            "height": "'.$this->height.'",
            "pattern": "'.$this->pattern.'"
        }';
    }

}