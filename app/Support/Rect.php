<?php

namespace App\Support;
use App\Support\FilteringClass;

class Rect
{
    var $id;
    var $width;
    var $height;
    var $landscape;
    var $x;
    var $y;
    var $curtain_id;
    var $list_of_not_fitting;
    
    function __construct($id, $width, $height, $curtain_id, $x=null,$y=null,$landscape=null)
    {
      $this->id = $id;
      $this->width = intval($width);
      $this->height = intval($height);
      $this->curtain_id = intval($curtain_id);
      $this->x = intval($x);
      $this->y = intval($y);
      $this->landscape = $landscape;
    }

    public function setCoordDirect( $xValue, $yValue, $landValue )
    {
		$this->x = $xValue;
		$this->y = $yValue;
		$this->landscape = $landValue;
    }

    public function get_area()
    {
		    return intval($this->width) * intval($this->height);
    }

    public function clone()
    {
		    return new Rect($this->id, $this->width, $this->height, $this->curtain_id, $this->x, $this->y, $this->landscape);
    }

    public function __toString()
    {
        return "ID:".$this->id." w:".$this->width." - h:".$this->height." - x:".$this->x." - y:".$this->y;
    }
           
    public function turn_into_landscape()
    {
        $this->landscape = True;
        $temp = $this->width;
        $this->width = $this->height;
        $this->height = $temp;
    }


    public function get_width_html()
    {
        return intval($this->width) / 10;
    }

    public function get_height_html()
    {
		return intval($this->height) / 10;
    }
	
    public function get_x_html()
    {
        return intval($this->x) / 10;
    }

    public function get_y_html()
    {
        return intval($this->y) / 10;
    }

    private function serialize( $obj )
    {
        return '{
          "id": '.$obj->id.',
          "width": "'.$obj->width.'",
          "height": "'.$obj->height.'",
          "x": "'.$this->x.'",
          "y": "'.$this->y.'",
          "landscape": "'.$this->landscape.'"
        }';
    }
    
    

    public function elaborate_list_of_not_fitting( $base, $curtains )
    {        
        $map_array = function( $el )
        {
          return $el->id;
        };

        $filtering_builder = new FilteringClass();
        $filtered = array_filter($curtains, $filtering_builder->not_fitting_filter_function_build($this, $base));
        
        $this->list_of_not_fitting = array_map($map_array,$filtered);
    }
}
    