<?php

namespace App\Processing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\CurtainsSetting;
use App\Support\Base;
use App\Support\Filling;
use App\Support\FilteringClass;

class PagesFillingsProcess
{   
    var $best_global_fitting;
    private function merge_splitting_trash( $trash_one, $trash_two, $class_type)
    {
        if ($trash_one->get_area == 0)
            return array(
                "new_util" => $trash_two,
                "new_trash" => 0
            );
        if ($trash_two->get_area == 0)
            return array(
                "new_util" => $trash_one,
                "new_trash" => 0
            );
        if ($class_type == 'classT')
            if ($trash_one->x + $trash_one->width == $trash_two->x)
                if ($trash_one->height >= $trash_two->height)
                {

                    $min_height = min($trash_one->height, $trash_two->height);
                    $new_util = new Base($trash_one->width + $trash_two->width, $min_height, $trash_one->pattern, $trash_one->x, $trash_one->y + ($trash_one->height - $min_height));
                    if ($trash_one->height < $trash_two->height)
                        $new_trash = new Base($trash_two->width, $trash_two->height - $min_height, $trash_two->pattern, $trash_two->x, $trash_two->y);
                    else{
                        $new_trash = new Base($trash_one->width, $trash_one->height - $min_height, $trash_one->pattern, $trash_one->x, $trash_one->y);
                        return array(
                            "new_util" => $new_util,
                            "new_trash" => $new_trash->get_area()
                        );
                    }
                }
        elseif ($class_type == 'classL')
            if ($trash_one->y + $trash_one->height == $trash_two->y)
                if ($trash_one->width >= $trash_two->width)
                {
                    $min_width = min($trash_one->width, $trash_two->width);
                    $new_util = new Base($min_width, $trash_one->height + $trash_two->height, $trash_one->pattern, $trash_one->x + ($trash_one->width - $min_width), $trash_one->y);
                    $new_trash = new Base($trash_one->width - $min_width, $trash_one->height, $trash_one->pattern, $trash_one->x, $trash_one->y);
                    return array(
                        "new_util" => $new_util,
                        "new_trash" => $new_trash->get_area()
                    );
                }
        return array(
            "new_util" => null,
            "new_trash" => $trash_one->get_area() + $trash_two->get_area()
        );
    }
        
    private function clone_array( $arr )
    {
        $new_arr = array();
        foreach ($arr as $el)
            array_push($new_arr, $el->clone());
        return $new_arr;
    }

    private function find_the_best_on_specific_base($this_base, $new_curtains)
    {
        $filtering_builder = new FilteringClass();
        
        $base_best = new Filling(null, 0, $this_base->get_area());
        if ($this_base->get_area() > 0){
            $prev_curt = null;
            foreach ($new_curtains as $check_curt)
            {
                if ($prev_curt == null || $prev_curt->width != $check_curt->width || $prev_curt->height != $check_curt->height)
                {
                    $curtains = array();
                    foreach ($new_curtains as $temp)
                    {
                        if ($temp->id != $check_curt->id)
                        array_push($curtains, $temp);
                    }
                    
                    $new_check_curt = $check_curt->clone();
                    if ($this_base->width >= $new_check_curt->width && $this_base->height >= $new_check_curt->height)
                    {
                        $new_check_curt->setCoordDirect( $this_base->x, $this_base->y, false );
                        
                        $temp_base_best = $this->findBestFilling($this_base, $new_check_curt, $curtains);

                        if($temp_base_best->trash < $base_best->trash){
                            $base_best = $temp_base_best;
                        }
                    }
                    //now check The same but 
                    if ($check_curt->width != $check_curt->height)
                    {
                        $new_landscape_check_curt = $check_curt->clone();

                        if ($this_base->width >= $new_landscape_check_curt->width && $this_base->height >= $new_landscape_check_curt->height)
                        {
                            $new_landscape_check_curt->setCoordDirect( $this_base->x, $this_base->y, false );
                            
                            $temp_base_best = $this->findBestFilling($this_base, $new_landscape_check_curt, $curtains);
                            if($temp_base_best->trash < $base_best->trash){
                                $base_best = $temp_base_best;
                            }   
                        }
                    }
                }
                
                $prev_curt = $check_curt;
            }
        }
        return $base_best;
    }

    private function get_from_two_bases($new_curtains, $base_one, $base_two)
    {
        $filtering_builder = new FilteringClass();
        $base_one_best = new Filling(null, 0, 0);
        if ($base_one->get_area() > 0)
            $base_one_best = $this->find_the_best_on_specific_base($base_one, $new_curtains);
        
        //Now on $base_one_best I have the best filling for that base let's filter the curtain list and check on th esecond base

        $temp_curtains_list = $new_curtains;
        if ($base_one_best->curtains != null)
            foreach($base_one_best->curtains as $temp_curt){
                $temp_curtains_list = array_filter($temp_curtains_list, $filtering_builder->all_but_one_filter_function_build($temp_curt));
            }
        
        $base_two_best = new Filling(null, 0, 0);
        if ($base_two->get_area() > 0)
            $base_two_best = $this->find_the_best_on_specific_base($base_two, $temp_curtains_list);
  
        $return_curtains = array();
        if ($base_one_best->curtains != null)
            foreach($base_one_best->curtains as $temp_curt)
                array_push($return_curtains, $temp_curt);
        
        if ($base_two_best->curtains != null)
            foreach($base_two_best->curtains as $temp_curt)
                array_push($return_curtains, $temp_curt);
        
        return new Filling($return_curtains, 0, $base_one_best->util + $base_two_best->util + $base_one_best->trash + $base_two_best->trash);
    }

    private function get_this_split_best_filling($split, $curtains)
    {
        if ($split->base_one->get_area() > 0 || $split->base_two->get_area() > 0){

            $this_based_best_filling_way_one = $this->get_from_two_bases($curtains, $split->base_one, $split->base_two);
            $this_based_best_filling_way_two = $this->get_from_two_bases($curtains, $split->base_two, $split->base_one);
            
            if ($this_based_best_filling_way_one->trash < $this_based_best_filling_way_two->trash)
                return $this_based_best_filling_way_one;
            else
                return $this_based_best_filling_way_two;
        }
        //they both have area = 0
        return new Filling(null, 0, 0);
    }

    /**
     * This Function needs a $base, a $current curtain already located, and a list of curtains that need to be inserted in the base
     * return the list of curtain for that filling included the current curt
     * @return array(\App\Support\Filling)
     */
    private function findBestFilling($base, $current, $curtains)
    {
        set_time_limit(30);
        
        $best_filling = new Filling(array(), 0, $base->get_area() - $current->get_area());
        if ($base->get_area() - $current->get_area() >= 0)
        {
            if(count($curtains) > 0)
            {
                $list_of_best_filling = array($current);
                $prevCurt = null;
                $splitting = $base->split_on_rect($current);
                $found = false;
                $curtains_count = count($curtains);
                foreach ($splitting as $split)
                {
                    if (!$found){

                        $temporary_filling = $this->get_this_split_best_filling($split, $curtains);
                        if ($best_filling->trash > $temporary_filling->trash)
                            $best_filling = $temporary_filling;
                        if ( count($best_filling->curtains) == $curtains_count)
                           $found = true;
                    }
                }
                    
                if ($best_filling->curtains != null)
                    foreach($best_filling->curtains as $temp)
                        array_push($list_of_best_filling, $temp);
                $best_filling->curtains = $list_of_best_filling;
            }
        }
        $ret_list = array($current);
        foreach ($best_filling->curtains as $curt)
            array_push($ret_list, $curt);
        
        $best_filling->curtains = $ret_list;

        return $best_filling;
    }

    private function filter_array_with_id_not_in_range( $element_list, $range_ids)
	{
        $ret_arr = array();
        foreach ($element_list as $el)
            if (!in_array($el->id,$range_ids) )
                array_push($ret_arr, $el);
        return $ret_arr;
    }

    private function get_fitting_curt_list( $total_area, $curtains, $list_of_not_fitting)
    {
        $ret_list = array();
        if ($total_area > 0)
        {
            $prevCurtCurtainId = -1;
            $i = 0;
            foreach ($curtains as $curt)
            {
                if (!in_array($curt->curtain_id, $list_of_not_fitting))
                {
                    if ($prevCurtCurtainId != $curt->curtain_id)
                    {
                        $prevCurtCurtainId = $curt->curtain_id;
                        if ($curt->get_area() <= $total_area)
                        {
                            $temp_list = array($curt);
                            $other_curtains = $this->get_fitting_curt_list($total_area - $curt->get_area(), array_slice($curtains,$i+1, count($curtains)), array_merge($list_of_not_fitting, $curt->list_of_not_fitting));
                            if (count($other_curtains) == 0){
                                array_push($ret_list, new Filling($temp_list, 0, $total_area - $temp_list[0]->get_area()));
                            } else{
                                foreach ($other_curtains as $one_list)
                                {
                                    $temp_temp_list = array_merge($temp_list, $one_list->curtains);
                                    array_push($ret_list, new Filling($temp_temp_list, 0, $one_list->trash));
                                }
                            }
                        }
                    }
                }
                $i++;
            }
        }
        return $ret_list;
    }

    private function find_best_fitting( $base, $curtains)
    {
        $list_of_filling_scheme = array();
        $curtains_included = array();
        foreach ($curtains as $current_curt)
        {  
            //If not already inserted in the scheme
            if (!in_array($current_curt->id, $curtains_included))
            {
                //I am gonna include the current Curtain
                $curtains_included = array_merge($curtains_included, [$current_curt->id]);
                //Get The Remain Curtains not inserted
                $filtered_array = $this->filter_array_with_id_not_in_range( $curtains, $curtains_included);
                //Get The Curtains Combination with the current one
                $curtains_to_check = $this->get_fitting_curt_list(($base->get_area() - $current_curt->get_area()), $filtered_array, $current_curt->list_of_not_fitting );
                
                $all_the_filling = array();
                
                $best_filled = new Filling(array(), 0, $base->get_area());

                //If there's no Combination 
                if (count($curtains_to_check) == 0)
                {
                    $best_filled = new Filling(array(), 0, $base->get_area() - $current_curt->get_area());
                    array_push($all_the_filling, $best_filled );
                } else {
                    $sorting_compare = function( $a, $b){
                        return $a->trash - $b->trash;
                    };
                    usort($curtains_to_check, $sorting_compare);

                    $count = 0;
                    $total = count($curtains_to_check);
                    $found = false;
                    foreach ($curtains_to_check as $curts_filling)
                    {
                        if (!$found)
                        {
                            if ($curts_filling != null)
                            {
                                if ($best_filled->trash <= $curts_filling->trash)
                                {
                                    $found = True;
                                } else {
                                    $current_curt->setCoordDirect( $base->x, $base->y, false );
                                    $this_best = $this->findBestFilling($base, $current_curt, $curts_filling->curtains);

                                    if ($best_filled->trash > $this_best->trash)
                                        $best_filled = $this_best;
                                    
                                    if (!$base->pattern)
                                    {
                                        $current_piece = $current_curt->clone();
                                        $current_piece->turn_into_landscape();
                                        
                                        $this_best = $this->findBestFilling($base, $current_piece, $curts_filling->curtains);

                                        if ($best_filled->trash > $this_best->trash){
                                            $best_filled = $this_best;
                                        }
                                    }
                                }
                            }
                        }
                        $count++;
                    }
                    
                    array_push($list_of_filling_scheme, $best_filled);
                    $curt_ids = array();
                    if ($best_filled->curtains != null)
                    {
                        foreach($best_filled->curtains as $rec)
                            array_push($curt_ids, $rec->id);
                    }

                    $curtains_included = array_merge($curtains_included, $curt_ids);
                }
            }
        }
        return $list_of_filling_scheme;
    }



    /**
     * Calculate the Curtains Best Filling And Return True When it's Ready, False if errors
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calculate($curtains_setting)
    {
        if ($curtains_setting == null)
            return json_encode('{"error": true, "msg": "Nessun Ordine Associato a questo id"}');
        if ($curtains_setting->page_width <= 0 || $curtains_setting->page_height <= 0)
            return json_encode('{"error": true, "msg": "Dimensione del foglio errate"}');
        
        $base = new Base($curtains_setting->page_width, $curtains_setting->page_height, $curtains_setting->page_pattern, 0, 0);
        $curtains = $curtains_setting->get_curtains_list();
        foreach ($curtains as $rec)
        {
            $rec->elaborate_list_of_not_fitting($base, $curtains);
        }
        $curtains_setting->calculating = true;
        $curtains_setting->save();

        $best_filling_curtains = $this->find_best_fitting( $base, $curtains);
        
        $curtains_setting->curtains_best_filling = json_encode($best_filling_curtains);
        $curtains_setting->calculating = false;
        $curtains_setting->save();

        return json_encode('{"error": false }');;
    }
}

