<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\CurtainsSetting;
use App\Support\Rect;
use App\Support\Base;
use App\Jobs\CurtainsProcessing;
use App\Processing\PagesFillingsProcess;

class CurtainsSettingsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', array(
            'except' => ['index', 'show']
        ));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = CurtainsSetting::orderBy("created_at",'desc')->paginate(10);
        return view('curtains.index')->with('settings', $settings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info(old('width'));
        return view('curtains.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,array(
            'setting_name' => 'required',
            'page_width' => 'required',
            'page_height' => 'required'
        ));
        $setting = new CurtainsSetting();
        $setting->curtains_name = $request->input('setting_name');
        $setting->calculating = false;
        $setting->page_width = $request->input('page_width');
        $setting->page_height = $request->input('page_height');
        $setting->page_pattern = $request->input('page_with_pattern') != null;

        $widths = $request->input('width.*');
        $heights = $request->input('height.*');
        $quantities = $request->input('quantity.*');
        
        $list_of_info = array();
        $curtains = array();        
        $id_counter = 1;
        for ($i=0;$i < count($widths); $i++)
        {
            if ($widths[$i] != ""){
                $w = intval($widths[$i]);
                $h = intval($heights[$i]);
                $q = intval($quantities[$i]);
                if ($q > 0 && $w > 0 && $h > 0)
                {
                    array_push($list_of_info, array("curt_id" => ($i +1), "w" => $w, "h" => $h, "q" => $q, "area" => ($w*$h) ) );

                    for ($k=0; $k < $q;$k++)
                    {
                        array_push($curtains, new Rect($id_counter, $w, $h, ( $i+1 ), $x=null,$y=null,$landscape=null));
                        $id_counter ++;
                    }

                }
            }
        }
        $total_count = count($curtains);
        $count_fitting = 0;
        $base = new Base($setting->page_width, $setting->page_height, $setting->page_pattern, 0, 0);
        foreach ($curtains as $rec)
        {
            $rec->elaborate_list_of_not_fitting($base, $curtains);
            $count_fitting += ($total_count - count($rec->list_of_not_fitting));
        }
        $setting->combination_number = $count_fitting;
        $setting->curtains_form_json = json_encode($list_of_info);
        $setting->user_id = auth()->user()->id;
        $setting->save();
        return redirect('/curtains/'.$setting->id)->with('success', 'Ordine Creato');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting = CurtainsSetting::find($id);
        return view('curtains.show')->with('setting', $setting);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = CurtainsSetting::find($id);

        if (auth()->user()->id != $setting->user_id)
            return redirect('/curtains/'.$id)->with('error', 'Unauthorized Page');
            
        return view('curtains.edit')->with('setting', $setting);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting = CurtainsSetting::find($id);
        if($request->input('setting_name') !=null )
            $setting->curtains_name = $request->input('setting_name');
        $setting->calculating = false;
        if($request->input('page_width') != null)
            $setting->page_width = $request->input('page_width');
        if($request->input('page_height') != null)
            $setting->page_height = $request->input('page_height');
        $setting->page_pattern = $request->input('page_with_pattern') != null;
        $setting->curtains_best_filling = null;
        $widths = $request->input('width.*');
        $heights = $request->input('height.*');
        $quantities = $request->input('quantity.*');
        
        $list_of_info = array();
        $curtains = array();        
        $id_counter = 1;
        for ($i=0;$i < count($widths); $i++)
        {
            if ($widths[$i] != ""){
                $w = intval($widths[$i]);
                $h = intval($heights[$i]);
                $q = intval($quantities[$i]);
                if ($q > 0 && $w > 0 && $h > 0)
                {
                    array_push($list_of_info, array("curt_id" => ($i +1), "w" => $w, "h" => $h, "q" => $q, "area" => ($w*$h) ) );

                    for ($k=0; $k < $q;$k++)
                    {
                        array_push($curtains, new Rect($id_counter, $w, $h, ( $i+1 ), $x=null,$y=null,$landscape=null));
                        $id_counter ++;
                    }

                }
            }
        }

        $total_count = count($curtains);
        $count_fitting = 0;
        $base = new Base($setting->page_width, $setting->page_height, $setting->page_pattern, 0, 0);
        foreach ($curtains as $rec)
        {
            $rec->elaborate_list_of_not_fitting($base, $curtains);
            $count_fitting += ($total_count - count($rec->list_of_not_fitting));
        }
        $setting->combination_number = $count_fitting;

        $setting->curtains_form_json = json_encode($list_of_info);
        $setting->user_id = auth()->user()->id;
        $setting->save();

        return redirect('/curtains/'.$id)->with('success', 'Ordine Aggiornato');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = CurtainsSetting::find($id);  
        $setting->delete();
        return redirect("/dashboard");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calculate($id)
    {
        $setting = CurtainsSetting::find($id);  

        if (auth()->user()->id != $setting->user_id){
            $msg = 'Accesso non autorizzato';
            if (isset($_GET["ajax"]) && $_GET["ajax"] == 'yes')
                return json_encode(array(
                    "response_code" => 300,
                    "msg" => $msg
                ));
            else
                return redirect('/curtains/'.$id)->with('error', $msg);
        }

        if ($setting->calculating){
            $msg = "Questo ordine sto ancora processando, la preghiamo di attendere.. Grazie!";
            if (isset($_GET["ajax"]) && $_GET["ajax"] == 'yes')
                return json_encode(array(
                    "response_code" => 300,
                    "msg" => $msg
                ));
            else
                return redirect('/curtains/'.$id)->with('error', $msg);
        }

        $setting->calculating = true;
        $setting->failed = false;
        $setting->save();
        
        set_time_limit($setting->combination_number);

        //Create Thred To Calculate Curtains Position on the page
        //CurtainsProcessing::dispatch($setting);
        $curtain_process = new PagesFillingsProcess();
        try{
            $curtain_process->calculate($setting);
        }catch(Exception $e){
            $setting->calculating = false;
            $setting->failed = true;
            $setting->save();
            if (isset($_GET["ajax"]) && $_GET["ajax"] == 'yes')
                return json_encode(array(
                    "response_code" => 500,
                    "msg" => $e->getMessage()
                ));
            else
                return redirect('/curtains/'.$id)->with('error', $e->getMessage());
        }
        
        $setting->calculating = false;
        $setting->failed = false;
        $setting->save();

        if (isset($_GET["ajax"]) && $_GET["ajax"] == 'yes')
            return json_encode(array(
                "response_code" => 200
            ));
        else
            return redirect('/curtains/'.$id)->with('success', 'Calcolo concluso con successo');
    }

    public function complete($id){
        return redirect('/curtains/'.$id)->with('success', 'Calcolo concluso con successo');
    }

    public function error($id){
        return redirect('/curtains/'.$id)->with('error', $_GET['msg']);
    }
}
