<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurtainsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curtains_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('curtains_name');
            $table->string('page_width');
            $table->string('page_height');
            $table->boolean('page_pattern');
            $table->mediumText('curtains_form_json');
            $table->mediumText('curtains_best_filling')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curtains_settings');
    }
}
